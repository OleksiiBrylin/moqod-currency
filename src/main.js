import Vue from 'vue'
import App from './App.vue'
import store from './store'

import {library} from '@fortawesome/fontawesome-svg-core'
import {faCog, faCogs, faSyncAlt} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

library.add(faCog, faCogs, faSyncAlt)

Vue.component('font-awesome-icon', FontAwesomeIcon)

import axios from 'axios'

axios.interceptors.request.use(request => {
    store.commit('LOADING', true);
    return request
})

axios.interceptors.response.use(response => {
        store.commit('LOADING', false);
        return response;
    }, error => {
        store.commit('LOADING', false);
        store.commit('ADD_ERROR', error);
        return Promise.reject(error);
    }
);

Vue.config.productionTip = false

new Vue({
    store,
    render: h => h(App)
}).$mount('#app')
