import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loading: false,
        errors: [],
        enabledIds: ['usd', 'eur'],
        currencyIds: ['usd', 'eur', 'cad', 'jpy'],
        currency: {
            usd: {
                id: 'usd',
                iso: 'USD',
                rate: null,
                title: 'US dollar'
            },
            eur: {
                id: 'eur',
                iso: 'EUR',
                rate: null,
                title: 'Euro'
            },
            cad: {
                id: 'cad',
                iso: 'CAD',
                rate: null,
                title: 'Canadian dollar'
            },
            jpy: {
                id: 'jpy',
                iso: 'JPY',
                rate: null,
                title: 'Japanese yen'
            },
        }
    },
    mutations: {
        LOADING: function (state, status) {
            state.loading = status;
        },
        SET_ENABLED_IDS: function (state, ids) {
            state.enabledIds = ids;
        },
        SET_RATE: function (state, payload) {
            state.currency[payload.id].rate = payload.rate;
        },
        ADD_ERROR: function (state, error) {
            state.errors.push(error);
        },
    },
    actions: {
        updateCurrency({commit, getters}, payload) {
            axios.get('https://www.cbr-xml-daily.ru/daily_json.js')
                .then((response) => {
                    let data = response.data.Valute;
                    if (typeof payload !== 'undefined') {
                        commit('SET_RATE', {
                            id: payload.id,
                            rate: data[payload.iso].Value
                        });
                        return;
                    }

                    getters.currency(true).forEach((item) => {
                        commit('SET_RATE', {
                            id: item.id,
                            rate: data[item.iso].Value
                        });
                    });
                }).catch((error) => {
                console.log(error);
            })
        },
    },
    getters: {
        loading(state) {
            return state.loading;
        },
        errors(state) {
            return state.errors;
        },
        lastError(state) {
            return state.errors[state.errors.length - 1];
        },
        allCurrency(state) {
            return state.currency;
        },
        enabledIds(state) {
            return state.enabledIds;
        },
        currency: (state) => (showAll) => {
            let ids = showAll ? state.currencyIds : state.enabledIds;

            return ids.map(id => state.currency[id]);
        }

    }
})
