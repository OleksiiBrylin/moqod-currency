# Moqod - Currency Application

Demo: https://moqod.herokuapp.com/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
node server.js
```

## Links
```
Bitbucket: https://bitbucket.org/OleksiiBrylin/moqod-currency.git
Demo: https://moqod.herokuapp.com/
```
